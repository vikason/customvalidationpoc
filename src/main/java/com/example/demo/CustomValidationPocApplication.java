package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@SpringBootApplication
public class CustomValidationPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomValidationPocApplication.class, args);
	}

}

@RestController
@Slf4j
class HelloResource {

	@PostMapping("/test")
	public String test(@RequestBody @Valid User user) {
		log.info("user data: {}", user);
		return user.toString();
	}


}

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class User {
	@ValidId
	private String userId;
}