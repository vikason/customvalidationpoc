package com.example.demo;

import lombok.extern.slf4j.Slf4j;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.util.regex.Pattern;

@Documented
@Constraint(validatedBy = ValidId.ValidIdImpl.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidId {

    String message() default "this is default message.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Slf4j
    class ValidIdImpl implements ConstraintValidator<ValidId, String> {

        @Override
        public void initialize(ValidId validId) {
            //doNothing.
        }

        @Override
        public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
            final String exp ="^((?:[a-zA-Z_](-)*))*$";
            log.info("Validating: {}",s);
            Pattern compile = Pattern.compile(exp);
            return compile.matcher(s).find();
        }
    }
}
